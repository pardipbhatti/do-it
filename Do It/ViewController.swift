//
//  ViewController.swift
//  Do It
//
//  Created by Pardip Bhatti on 09/04/17.
//  Copyright © 2017 gpCoders. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var tasks: [Task] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // Reload data on delete or add
    override func viewWillAppear(_ animated: Bool) {
        getTasks()
        tableView.reloadData()
    }

    // Return number of rows of table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    // Pass data in table rows
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let task = tasks[indexPath.row];
        
        if(task.priority) {
            cell.textLabel?.text = "❗️\(task.name!)"
        } else {
            cell.textLabel?.text = task.name!
        }
        return cell
    }
    
    // Sending data to details view
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let task = tasks[indexPath.row]
        performSegue(withIdentifier: "details", sender: task)
    }
    
    // Moving to addNote View controller
    @IBAction func addNote(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "addNote", sender: "gugu")
    }
    
    // Getting all notes form coredata
    func getTasks() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            tasks = try context.fetch(Task.fetchRequest()) as! [Task]
        } catch {
            print("We have an error")
        }
        
    }
    
    // Sending object to DetaisViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "details" {
            let taskDetails = segue.destination as! DetailsViewController
            taskDetails.detailVC = sender as? Task
        }
    }
    
    // Swipe Delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            context.delete(tasks[indexPath.row])
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            tasks.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .left)
        }
    }
    
    
    
    
}

