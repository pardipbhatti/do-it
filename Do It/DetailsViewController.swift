//
//  DetailsViewController.swift
//  Do It
//
//  Created by Pardip Bhatti on 09/04/17.
//  Copyright © 2017 gpCoders. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var detailsVC: UILabel!
    
    var detailVC: Task? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Details()
    }

    
    func Details() -> () {
        if(detailVC!.priority) {
            detailsVC.text = "!\(detailVC!.name!)"
        } else {
            detailsVC.text = detailVC!.name!
        }
    }

    @IBAction func complete(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        context.delete(detailVC!)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        navigationController?.popViewController(animated: true)
    }
}
