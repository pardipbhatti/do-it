//
//  AddNoteViewController.swift
//  Do It
//
//  Created by Pardip Bhatti on 09/04/17.
//  Copyright © 2017 gpCoders. All rights reserved.
//

import UIKit

class AddNoteViewController: UIViewController {

    
    
    @IBOutlet weak var tastTextField: UITextField!
    @IBOutlet weak var priority: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func addNotes(_ sender: Any) {
        // Create a note
        let context = (UIApplication.shared.delegate as! AppDelegate)
    
        let task = Task(context: context.persistentContainer.viewContext)
        
        task.name = tastTextField.text!
        task.priority = priority.isOn
        context.saveContext()
        
        // Pop Back
        navigationController?.popViewController(animated: true)
    }
    

}
